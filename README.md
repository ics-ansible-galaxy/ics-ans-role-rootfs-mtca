ics-ans-role-rootfs-mtca
===================

Ansible role to install rootfs-mtca.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rootfs-mtca
```

License
-------

BSD 2-clause
